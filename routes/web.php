<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/////////////////////
//is admin
//Route::get('protected', ['middleware' => ['auth', 'admin'], function() {
//    return "this page requires that you be logged in and an Admin";
//}]);

// OR

//Route::get('admin', ['as' =>'admin.page', 'uses' => 'PagesController@admin', 'middleware' => ['auth', 'admin']]);
//
//Route::get('protected', ['as' =>'secure.page', 'uses' => 'PagesController@secure', 'middleware' => 'auth']);
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//test file upload
Route::get('/import/upload', 'Imports\CsvImportController@form')->name('upload.form');
Route::post('/import/upload', ['as' => 'fileUpload', 'uses' => 'Imports\CsvImportController@upload'])->name('upload.upload');
//to import from campaign creation form.
Route::get('/import/upload/{campaignUUID?}', 'Imports\CsvImportController@form')->name('upload.form.uuid');
Route::post('/import/upload/{campaignUUID?}', ['as' => 'fileUpload', 'uses' => 'Imports\CsvImportController@upload'])->name('upload.upload.uuid');
Route::post('/import/link', 'Imports\CsvImportController@link')->name('upload.link');
//Route::post('/import/link', 'Imports\CsvImportController@link')->name('upload.link');


//streams
//Route::get('/streams/render', 'Streams\StreamController@render')->name('stream.render');
//Route::get('/streams/formBuilder', 'Streams\StreamController@formBuilder')->name('stream.formBuilder');
//Route::get('/streams/tohtml/{slug?}', 'Streams\StreamController@toHtml')->name('stream.tohtml');

//custom fields (there's a difference between streams, witch are the internal logics, and custom_fields, more HMI oriented) :
Route::get('/streams/customFieldBuilder', 'Streams\StreamMakerController@createCustomFieldForm')->name('customfield.formBuilder');
Route::post('/streams/customFieldBuilder', 'Streams\StreamMakerController@store')->name('customfield.store');
Route::get('/streams/ajaxCheckSlug/{slug?}', 'Streams\StreamMakerController@ajaxCheckSlugIsUnique')->name('customfield.ajaxCheckSlugIsUnique');
Route::get('/streams/index', 'Streams\StreamMakerController@index')->name('customfield.index');
Route::get('/streams/customFieldBuilder/{slug?}', 'Streams\StreamMakerController@updateCustomFieldForm')->name('customfield.formBuilder');
Route::post('/streams/customFieldBuilder/{slug?}', 'Streams\StreamMakerController@updateCustomFieldStore')->name('customfield.update.store');
Route::delete('/streams/customFieldBuilder', 'Streams\StreamMakerController@delete')->name('customfield.delete');
Route::post('/streams/ajaxRender', 'Streams\StreamMakerController@postRender')->name('customfield.ajaxRender');



//CRUD users
Route::get('/admin/users','UserMgt\UserMgtController@index')->name('admin.users.index');
Route::delete('/admin/users','UserMgt\UserMgtController@delete')->name('admin.users.delete');
Route::patch('/admin/users','UserMgt\UserMgtController@update')->name('admin.users.update');
Route::post('/admin/users', 'UserMgt\UserMgtController@create')->name('admin.users.create');
Route::get('/admin/users/fake', 'UserMgt\UserMgtController@fakeUsers')->name('admin.users.fake');

//CRUD Campaigns
Route::get('/admin/campaigns/create', 'CampaignMgt\CampaignController@creationForm')->name('admin.campaigns.create');
Route::get('/campaigns/render', 'CampaignMgt\CampaignController@render')->name('admin.campaigns.render');
Route::post('/campaigns/testsubmit', 'CampaignMgt\CampaignController@store')->name('admin.campaigns.store');

//formrenderer
Route::get('/campaigns/render2', 'Forms\FormController@index')->name('form.read.index');


//campaign creation wizard
//step1 :
Route::get('/admin/campaigns/wizard/1', 'Campaigns\WizardController@step1')->name('admin.campaigns.wizard.1');
Route::post('/admin/campaigns/wizard/2', 'Campaigns\WizardController@step2')->name('admin.campaigns.wizard.2');
Route::post('/admin/campaigns/wizard/3', 'Campaigns\WizardController@step3')->name('admin.campaigns.wizard.3');
Route::post('/admin/campaigns/wizard/4', 'Campaigns\WizardController@step4')->name('admin.campaigns.wizard.4');





