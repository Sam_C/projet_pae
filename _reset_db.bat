@echo off
if exist "database\database.sqlite.bak" del "database\database.sqlite.bak"

if exist "database\database.sqlite" move "database\database.sqlite" "database\database.sqlite.bak"

copy "_database.sqlite" "database\database.sqlite"
pause
