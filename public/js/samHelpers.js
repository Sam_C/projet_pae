/**
 * Created by Sam on 19/07/2017.
 */
function ajaxPreviewStream(csrf_token, slug, outputContainer) {

    $.ajax({
        url: "/streams/ajaxRender",
        method: "POST",
        async: true,
        data: {
            _token: csrf_token,
            'slug': slug
        },


        success: function (msg) {

            html = (msg.replace(/\\/g, ''));
            html = (html.trim());
            html = html.substring(1, html.length - 1);

            outputContainer.html((html));

        },

        done: (function (msg) {

        }),

        fail: (function (jqXHR, textStatus) {

        }),
        error: (function (msg) {
            alert('Quelque chose s\'est mal passé');
        })
    });


};