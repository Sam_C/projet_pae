{{--{{dump(request())}}--}}

{{--header--}}
@section('logo-mini-caption') Adm @endsection
@section('logo-maxi-caption') Admin @endsection

@section('header-messages-dropdown')
    {{--   <li class="dropdown messages-menu">
           <!-- Menu toggle button -->
           <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               <i class="fa fa-envelope-o"></i>
               <span class="label label-success">4</span>
           </a>
           <ul class="dropdown-menu">
               <li class="header">You have 4 messages</li>
               <li>
                   <!-- inner menu: contains the messages -->
                   <ul class="menu">
                       <li><!-- start message -->
                           <a href="#">
                               <div class="pull-left">
                                   <!-- User Image -->
                                   <img src="adminlte/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                               </div>
                               <!-- Message title and timestamp -->
                               <h4>
                                   Support Team
                                   <small><i class="fa fa-clock-o"></i> 5 mins</small>
                               </h4>
                               <!-- The message -->
                               <p>Why not buy a new awesome theme?</p>
                           </a>
                       </li>
                       <!-- end message -->
                   </ul>
                   <!-- /.menu -->
               </li>
               <li class="footer"><a href="#">See All Messages</a></li>
           </ul>
       </li>
       <!-- /.messages-menu -->

       <!-- Notifications Menu -->
       <li class="dropdown notifications-menu">
           <!-- Menu toggle button -->
           <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               <i class="fa fa-bell-o"></i>
               <span class="label label-warning">10</span>
           </a>
           <ul class="dropdown-menu">
               <li class="header">You have 10 notifications</li>
               <li>
                   <!-- Inner Menu: contains the notifications -->
                   <ul class="menu">
                       <li><!-- start notification -->
                           <a href="#">
                               <i class="fa fa-users text-aqua"></i> 5 new members joined today
                           </a>
                       </li>
                       <!-- end notification -->
                   </ul>
               </li>
               <li class="footer"><a href="#">View all</a></li>
           </ul>
       </li>
       <!-- Tasks Menu -->
       <li class="dropdown tasks-menu">
           <!-- Menu Toggle Button -->
           <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               <i class="fa fa-flag-o"></i>
               <span class="label label-danger">9</span>
           </a>
           <ul class="dropdown-menu">
               <li class="header">You have 9 tasks</li>
               <li>
                   <!-- Inner menu: contains the tasks -->
                   <ul class="menu">
                       <li><!-- Task item -->
                           <a href="#">
                               <!-- Task title and progress text -->
                               <h3>
                                   Design some buttons
                                   <small class="pull-right">20%</small>
                               </h3>
                               <!-- The progress bar -->
                               <div class="progress xs">
                                   <!-- Change the css width attribute to simulate progress -->
                                   <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"
                                        aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                       <span class="sr-only">20% Complete</span>
                                   </div>
                               </div>
                           </a>
                       </li>
                       <!-- end task item -->
                   </ul>
               </li>
               <li class="footer">
                   <a href="#">View all tasks</a>
               </li>
           </ul>
       </li>--}}
@endsection

@section('header-user-menu')
    <li class="dropdown user user-menu">
        <!-- Menu Toggle Button -->
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <!-- The user image in the navbar-->
            <img src="{{asset('admin/adminlte/dist/img/user-icon-black.png')}}" class="user-image" alt="User Image">
            <!-- hidden-xs hides the username on small devices so only the image appears. -->
            <span class="hidden-xs">
                @if(\Illuminate\Support\Facades\Auth::user() != null)
                    {{\Illuminate\Support\Facades\Auth::user()->firstname}} {{\Illuminate\Support\Facades\Auth::user()->lastname}}
                @else
                    Un utilisateur
                @endif
            </span>

        </a>
        <ul class="dropdown-menu">
            <!-- The user image in the menu -->
            <li class="user-header">
                <img src="{{asset('admin/adminlte/dist/img/user-icon-black.png')}}" class="img-circle" alt="User Image">

                <p>
                    @if(\Illuminate\Support\Facades\Auth::user() != null)
                        {{\Illuminate\Support\Facades\Auth::user()->company}}
                    @else
                        <small>une société</small>
                    @endif

                </p>
            </li>
            <!-- Menu Body -->
            <li class="user-body">
                <div class="row">
                    <div class="col-xs-4 text-center">
                        <a href="#">Un truc</a>
                    </div>
                    <div class="col-xs-4 text-center">
                        <a href="#">un autre</a>
                    </div>
                    <div class="col-xs-4 text-center">
                        <a href="#">et encore un</a>
                    </div>
                </div>
                <!-- /.row -->
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
                <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                    <a href="{{route('logout')}}" class="btn btn-default btn-flat">Se déconnecter</a>
                </div>
            </li>
        </ul>
    </li>
@endsection


@section('user-sidebar-infos')
    <p>@if(\Illuminate\Support\Facades\Auth::user() != null)
            {{\Illuminate\Support\Facades\Auth::user()->firstname}} {{\Illuminate\Support\Facades\Auth::user()->lastname}}
        @else
            Un utilisateur
        @endif</p>
    <!-- Status -->
    @if(\Illuminate\Support\Facades\Auth::user() != null)

        @if(\Illuminate\Support\Facades\Auth::user()->level == 1)
            <a href="#"><i class="fa fa-circle text-success"></i> Administrateur</a>
        @else
            <a href="#"><i class="fa fa-circle text-info"></i> Utilisateur</a>
        @endif
    @endif
    {{--<a href="#"><i class="fa fa-circle text-success"></i> En ligne</a>--}}
@endsection


@section('footer')
    @include('adminlte.footer')
@endsection

@section('controlsidebar')
    @include('adminlte.controlsidebar')
@endsection

{{--main body--}}

{{--footer--}}
@section('footer-left')
    un texte à gauche
@endsection

@section('footer-right')
    un à droite
@endsection