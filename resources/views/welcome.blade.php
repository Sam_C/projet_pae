<?php

use Mexitek\PHPColors\Color;

$classes = ['bounce',
        'flash',
        'pulse',
        'rubberBand',
        'shake',
        'headShake',
        'swing',
        'tada',
        'wobble',
        'jello',
        'bounceIn',
        'bounceInDown',
        'bounceInLeft',
        'bounceInRight',
        'bounceInUp',
//            'bounceOut',
//            'bounceOutDown',
//            'bounceOutLeft',
//            'bounceOutRight',
//            'bounceOutUp',
        'fadeIn',
        'fadeInDown',
        'fadeInDownBig',
        'fadeInLeft',
        'fadeInLeftBig',
        'fadeInRight',
        'fadeInRightBig',
        'fadeInUp',
        'fadeInUpBig',
//            'fadeOut',
//            'fadeOutDown',
//            'fadeOutDownBig',
//            'fadeOutLeft',
//            'fadeOutLeftBig',
//            'fadeOutRight',
//            'fadeOutRightBig',
//            'fadeOutUp',
//            'fadeOutUpBig',
        'flipInX',
        'flipInY',
//            'flipOutX',
//            'flipOutY',
        'lightSpeedIn',
//            'lightSpeedOut',
        'rotateIn',
        'rotateInDownLeft',
        'rotateInDownRight',
        'rotateInUpLeft',
        'rotateInUpRight',
//            'rotateOut',
//            'rotateOutDownLeft',
//            'rotateOutDownRight',
//            'rotateOutUpLeft',
//            'rotateOutUpRight',
        'hinge',
        'jackInTheBox',
        'rollIn',
//            'rollOut',
        'zoomIn',
        'zoomInDown',
        'zoomInLeft',
        'zoomInRight',
        'zoomInUp',
//            'zoomOut',
//            'zoomOutDown',
//            'zoomOutLeft',
//            'zoomOutRight',
//            'zoomOutUp',
        'slideInDown',
        'slideInLeft',
        'slideInRight',
        'slideInUp',
//            'slideOutDown',
//            'slideOutLeft',
//            'slideOutRight',
    //'slideOutUp',
];

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Homepage</title>

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

</head>
<body style="background-color: #001a35">

<div style="width: 99%">
    <div class="<?php echo $classes[array_rand($classes, 1)]; ?> animated"
         style="float: left; background-color: <?php
         $myColor = new Color('#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT));
         echo('#' . $myColor->getHex());
         ?>;
                 color:<?php echo '#' . $myColor->complementary()?>; padding-left: 10px; padding-right: 10px; width: 100%; -moz-text-decoration-color: white">
        <h1>Nice to see you !</h1>
    </div>
    <div class="flipInY animated"
         style="float: left;
                 background-color: <?php
         $myColor = new Color('#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT));
         echo('#' . $myColor->getHex());
         ?>;
                 color:<?php echo '#' . $myColor->complementary()?>;
                 padding-left: 10px;
                 padding-right: 10px">
        <h1>Why don't you come back here by following the link provided ?</h1>
    </div>

</div>

</body>
</html>


{{--<!doctype html>--}}
{{--<html lang="{{ app()->getLocale() }}">--}}
{{--<head>--}}
{{--<meta charset="utf-8">--}}
{{--<meta http-equiv="X-UA-Compatible" content="IE=edge">--}}
{{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}

{{--<title>Laravel</title>--}}

{{--<!-- Fonts -->--}}
{{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}

{{--<!-- Styles -->--}}
{{--<style>--}}
{{--html, body {--}}
{{--background-color: #fff;--}}
{{--color: #636b6f;--}}
{{--font-family: 'Raleway', sans-serif;--}}
{{--font-weight: 100;--}}
{{--height: 100vh;--}}
{{--margin: 0;--}}
{{--}--}}

{{--.full-height {--}}
{{--height: 100vh;--}}
{{--}--}}

{{--.flex-center {--}}
{{--align-items: center;--}}
{{--display: flex;--}}
{{--justify-content: center;--}}
{{--}--}}

{{--.position-ref {--}}
{{--position: relative;--}}
{{--}--}}

{{--.top-right {--}}
{{--position: absolute;--}}
{{--right: 10px;--}}
{{--top: 18px;--}}
{{--}--}}

{{--.content {--}}
{{--text-align: center;--}}
{{--}--}}

{{--.title {--}}
{{--font-size: 84px;--}}
{{--}--}}

{{--.links > a {--}}
{{--color: #636b6f;--}}
{{--padding: 0 25px;--}}
{{--font-size: 12px;--}}
{{--font-weight: 600;--}}
{{--letter-spacing: .1rem;--}}
{{--text-decoration: none;--}}
{{--text-transform: uppercase;--}}
{{--}--}}

{{--.m-b-md {--}}
{{--margin-bottom: 30px;--}}
{{--}--}}
{{--</style>--}}
{{--</head>--}}
{{--<body>--}}
{{--<div class="flex-center position-ref full-height">--}}
{{--@if (Route::has('login'))--}}
{{--<div class="top-right links">--}}
{{--@if (Auth::check())--}}
{{--<a href="{{ url('/home') }}">Home</a>--}}
{{--@else--}}
{{--<a href="{{ url('/login') }}">Login</a>--}}
{{--<a href="{{ url('/register') }}">Register</a>--}}
{{--@endif--}}
{{--</div>--}}
{{--@endif--}}

{{--<div class="content">--}}
{{--<div class="title m-b-md">--}}
{{--Laravel--}}
{{--</div>--}}


{{--<div class="links">--}}
{{--<a href="https://laravel.com/docs">Documentation</a>--}}
{{--<a href="https://laracasts.com">Laracasts</a>--}}
{{--<a href="https://laravel-news.com">News</a>--}}
{{--<a href="https://forge.laravel.com">Forge</a>--}}
{{--<a href="https://github.com/laravel/laravel">GitHub</a>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</body>--}}
{{--</html>--}}
