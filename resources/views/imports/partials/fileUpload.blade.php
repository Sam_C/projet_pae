@include('imports.partials.headerpart')
<div class="form-group">
    <form action="" enctype="multipart/form-data" method="post">
        {{csrf_field()}}
        <input type="hidden" value="{{$uuid}}" name="campaignUUID">
        <label for="file" class="control-label"></label>

        <input type="file" name="file" id="file" class="form-control btn btn-file btn-info file"
               data-allowed-file-extensions='["csv", "xls", xlsx]'><br>

        <button type="submit" value="upload" class="form-control btn btn-success">ok</button>
    </form>
</div>
@include('imports.partials.footerPart')


