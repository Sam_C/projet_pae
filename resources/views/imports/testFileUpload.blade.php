@extends('layouts.adminlte')
@include('adminlte_static_content')
@section('content-header')
    <h1>un titre !</h1>
    <p>Choisissez un fichier CSV, XLS ou XLSX à uploader afin de générer une liste d'invités.</p>
    <p>L'étape suivante vous demandera quels champs associer à quelle colonne.</p>
@endsection
@section('content-body')
    <div class="form-group">
        <form action="" enctype="multipart/form-data" method="post">
            {{csrf_field()}}

            <label for="file" class="control-label"></label>

            <input type="file" name="file" id="file" class="form-control btn btn-file btn-info file"
                   data-allowed-file-extensions='["csv", "xls", xlsx]'><br>

            <button type="submit" value="upload" class="form-control btn btn-success">ok</button>
        </form>
    </div>
@endsection
@section('aditionnal-scripts')

@endsection