{{--@foreach($data as $row)--}}
{{--{{dump($row[0])}}--}}
{{--@endforeach--}}
{{--{{dump($data["file"])}}--}}
@extends('layouts.adminlte')
@include('adminlte_static_content')

@section('content-header')
    <h1>un titre !</h1>
    <p>Merci de stipuler, dans l'entête du tableau suivant, le nom des colonnes à exploiter.</p>
    <h5>il est IMPERATIF de désigner la colonne contenant les adresses mails</h5>
    <p>Les colonnes non nommées ne seront pas importées.</p>
@endsection
@section('content-body')
    <datalist id="associateField">
        <option value="Firstname"></option>
        <option value="Lastname"></option>
        <option value="email"></option>
        <option value="tel"></option>
        <option value="company"></option>
    </datalist>

    <div class="table-responsive">
        <form action="{{route('upload.link')}}" method="post">
            {{csrf_field()}}
            <input name="list_name" type="hidden" value="{{$data["file"]}}">

            <table class="table table-hover">

                <thead>
                <tr>
                    @foreach($data['headers'] as $header)
                        <th>{{ucfirst($header)}}</th>
                    @endforeach
                </tr>
                <tr>


                    @foreach($data['headers'] as $k=>$header)
                        <th><input id="field{{$k}}" class="form-control" list="associateField" name="field{{$k}}"
                                   value="" onchange="setColumnName(this)"></th>
                    @endforeach

                </tr>
                </thead>
                <tbody>
                @foreach($data['rows'] as $r=>$row)
                    <tr>
                        @foreach($row as $c=>$cell)
                            <td style="text-align: center">{{$cell}}</td>
                        @endforeach
                    </tr>
                @endforeach
                <tr style="text-align: center">
                    @foreach($row as $c=>$cell)
                        <td> ...</td>
                    @endforeach
                </tr>
                </tbody>
            </table>
            <button type="submit" class="form-control btn btn-info">Valider</button>
        </form>
    </div>
@endsection
@section('aditionnal-scripts')

@endsection