@extends('layouts.adminlte')

@include('adminlte_static_content')
@section('content-header')
    <h1>un titre !</h1>


@endsection
@section('content-body')


    <form action="{{route('admin.campaigns.wizard.2')}}" method="POST" role="form">
        {{csrf_field()}}

        <legend>Création d'une campagne</legend>

        <div class="form-group">
            <label for="">Nom de la campagne</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Nom">
        </div>

        <div class="form-group">
            <label for="">Slug de la campagne</label>
            <input type="text" class="form-control" id="slug" name="slug" placeholder="Slug...">
        </div>

        <div class="form-group">
            <label for="">UUID de la campagne</label>
            <input type="text" class="form-control" id="UUID" name="UUID" placeholder="UUID..." value="{{$uuid}}"
                   readonly>
        </div>


        <button type="submit" class="btn btn-primary">Etape 2 -> importer des invités</button>
    </form>

@endsection

