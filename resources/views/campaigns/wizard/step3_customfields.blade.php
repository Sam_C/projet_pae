{{--{{dump($fields)}}--}}
@extends('layouts.adminlte')


@include('adminlte_static_content')
@section('content-header')
    <h1>un titre !</h1>


@endsection
@section('content-body')
    <div style="width: 100%">
        {{--this part of the page is the customfields store--}}
        <div style="width: 100%;display: inline-block">
            Champs persos
        </div>
        {{--here, we list every field the user can pick up and put back--}}
        <div id="myList" class="col-sm-10" style="background-color: #00a7d0; width: 100%;min-height: 35px">
            <?php $i = 0;?>
            @foreach($fields as $field)
                <div class="btn btn-adn" style="float: left;margin: 2px"
                     ondblclick="ajaxPreviewStream('{{csrf_token()}}','{{$field->slug}}',$('#myPreview'))">{{$field->name.' | '.$field->slug}}
                    <input type="hidden" value="{{$field->slug}}" name="field{{$i}}"></div>
                <?php $i++;?>
            @endforeach
        </div>
    </div>
    {{--this is the user's basket--}}
    <form action="{{route('admin.campaigns.wizard.4')}}" method="post">
        <div style="width: 100%;display: inline-block">
            Glissez votre sélection ci-dessous :
        </div>
        {{--Here, the user can drop any fields he needs, or drag it back to the customfield store if he did a mistake... --}}
        <div class="" style="width: 100%">
            <div id="mySelection" class="col-sm-10" style="background-color: #00a7d0; width: 100%; min-height: 35px">
                {{--we load already existing fields related to this campaign :--}}
                @foreach($campaignFields as $field)
                    <div class="btn btn-adn" style="float: left;margin: 2px"
                         ondblclick="ajaxPreviewStream('{{csrf_token()}}','{{$field->slug}}',$('#myPreview'))">{{$field->name.' | '.$field->slug}}
                        <input type="hidden" value="{{$field->slug}}" name="field{{$i}}"></div>
                    <?php $i++;?>
                @endforeach
            </div>
        </div>
        <div style="margin-top: 10px">
            {{csrf_field()}}
            <input type="hidden" name="UUID" value="{{$uuid}}">
            <button type="submit" class="form-control btn btn-info" style="margin-top: 10px">Etape 4 -> contenu de la
                campagne
            </button>
        </div>
    </form>

    <div id="myPreview">

    </div>
    {{--drag'n'drop--}}
    <script src="{{asset('js/dragula/dragula.js')}}"></script>
    <link rel="stylesheet" href="{{asset('js/dragula/dragula.css')}}">
    <script src="{{asset('js/samHelpers.js')}}"></script>

    <script>
        dragula([document.querySelector('#myList'), document.querySelector('#mySelection')], {ignoreInputTextSelection: true});
    </script>

@endsection
