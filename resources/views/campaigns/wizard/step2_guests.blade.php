@extends('layouts.adminlte')

<script>
    function resizeIframe(obj) {
        obj.style.height = obj.contentWindow.document.body.scrollHeight + 20 + 'px';
    }
</script>

@include('adminlte_static_content')
@section('content-header')
    <h1>un titre !</h1>


@endsection
@section('content-body')


    <iframe src="{{route('upload.form.uuid').'/'.$uuid}}" frameborder="0" scrolling="no"
            style="border: none; height: auto;width: 100%" ; onload="resizeIframe(this)"></iframe>
    <form action="{{route('admin.campaigns.wizard.3')}}" method="post">
        {{csrf_field()}}
        <input type="hidden" name="UUID" value="{{$uuid}}">
        <button class="form-control btn btn-success">Etape 3 -> choisir les champs personnalisés</button>
    </form>
@endsection

