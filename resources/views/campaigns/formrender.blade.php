@extends('layouts.app')
@section('content')
    {{--{{dump($elements)}}--}}
    @foreach($elements as $customFieldArray)
        {{--{{dump($customFieldArray)}}--}}
        @if(is_array($customFieldArray))@include('campaigns.partials.customfieldRender')@else{!! $customFieldArray !!}@endif
    @endforeach
@endsection