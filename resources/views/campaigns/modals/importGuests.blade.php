<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalAdd" style="width: 100%">Importer
    des invités
</button>


<div id="myModalAdd" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 90%">

        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Importer des invités</h4>
            </div>
            <div class="modal-body">
                <iframe src="{{route('upload.form.uuid',$myuuid)}}"
                        style="width: 100%;height: 70vh;border-style: none"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                <button type="reset" class="btn btn-warning">Réinit.</button>
                <button type="submit" class="btn btn-danger">OK</button>
            </div>


        </div>

    </div>

</div>
