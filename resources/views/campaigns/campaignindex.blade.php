@extends('layouts.adminlte')
{{--{{dump(\Illuminate\Support\Facades\Auth::user())}}--}}
{{--{{dump($errors)}}--}}
@if(old('_method') !== "PATCH")
    <style>

        table {
            /*margin-top: 25px;*/
            /*font-size: 21px;*/
            /*text-align: center;*/

            -webkit-animation: fadein 0.8s; /* Safari, Chrome and Opera > 12.1 */
            -moz-animation: fadein 0.8s; /* Firefox < 16 */
            -ms-animation: fadein 0.8s; /* Internet Explorer */
            -o-animation: fadein 0.8s; /* Opera < 12.1 */
            animation: fadein 0.8s;
        }

        @keyframes fadein {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }

        /* Firefox < 16 */
        @-moz-keyframes fadein {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }

        /* Safari, Chrome and Opera > 12.1 */
        @-webkit-keyframes fadein {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }

        /* Internet Explorer */
        @-ms-keyframes fadein {

        from {
            opacity: 0;
        }

        to {
            opacity: 1;
        }

        }

        /* Opera < 12.1 */
        @-o-keyframes fadein {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }


    </style>
@endif

@include('adminlte_static_content')
@section('content-header')
    <h1>un titre !</h1>


@endsection
@section('content-body')


    @if (Session::get('success'))

        <div class="alert alert-success">
            <ul>

                <li>{{ Session::get('success') }}</li>

            </ul>
        </div>
    @endif


    <table class="table table-hover" style="">
        {{ $data->links() }}

        <thead>
        <tr>
            <th>firstname</th>
            <th>lastname</th>
            <th>mail</th>
            <th>company</th>
            <th>level</th>
            <th>campaigns</th>
            <th colspan="2">
                @include('users.partials.usermgt_create_modal')

            </th>
            {{--<th></th><th></th>--}}
        </tr>
        </thead>

        @foreach($data as $userrecord)
            {{--{{dump($userrecord)}}--}}


            <tbody>
            <tr>

                <td>{{$userrecord->firstname}}</td>
                <td>{{$userrecord->lastname}}</td>
                <td>{{$userrecord->email}}</td>
                <td>{{$userrecord->company}}</td>

                <td>
                    @if($userrecord->level==1)
                        <img src="{{asset('admin/adminlte/dist/img/admin-icon.png')}}" height="30" width="30"
                             title="administrateur">
                    @else
                        <img src="{{asset('admin/adminlte/dist/img/user-icon-black.png')}}" height="30" width="30"
                             title="utilisateur">
                    @endif
                </td>
                <td>view campains</td>
                <td style="width: 35px; text-align: center">

                    @include('users.partials.usermgt_update_modal')
                </td>
                <td style="width: 35px; text-align: center">

                    @include('users.partials.usermgt_delete_modal')

                </td>
            </tr>
            </tbody>

        @endforeach
    </table>
    {{ $data->links() }}

@endsection
