@extends('layouts.adminlte')
{{--{{dump(\Illuminate\Support\Facades\Auth::user())}}--}}
{{--{{dump($errors)}}--}}

<style type="text/css" media="screen">
    .tab-pane {
        position: relative;
        height: 70%;
        min-height: 80%;
    }

    .MyEditor {
        position: absolute;
        height: 90%;
        min-height: 100%;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        /*height: 480px;*/
        /*width: 100%;*/
        /*bottom: 0;*/

    }
</style>

@include('adminlte_static_content')
@section('content-header')
    <h1>un titre !</h1>
@endsection


@section('content-body')

@include('campaigns.partials.navigation_header')
        <!-- general setting panel-->
    <form action="" method="post">
        {{csrf_field()}}
    <div class="tab-content">

        @include('campaigns.partials.panels.generalPanel.generalPanel')

        @include('campaigns.partials.panels.mailPanel.mailPanel')
                <!-- Form HTML panel-->
        @include('campaigns.partials.panels.formPanel.formPanel')
                <!-- thanks HTML panel-->
        @include('campaigns.partials.panels.thankPanel.thankPanel')
                <!-- Javascript panel-->
        @include('campaigns.partials.panels.javascriptPanel.javascriptPanel')
                <!-- CSS panel-->
        @include('campaigns.partials.panels.cssPanel.cssPanel')

        <div id="return" style="height: 800px;width: 100%;"></div>
    </div>
    </form>
@endsection
@section('aditionnal-scripts')

    @include('campaigns.partials.javascript')



@endsection
