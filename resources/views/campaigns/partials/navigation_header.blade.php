<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="pill" href="#general">Général</a></li>
    <li><a data-toggle="pill" href="#mail">Mail</a></li>
    <li><a data-toggle="pill" href="#formeditor">Formulaire</a></li>
    <li><a data-toggle="pill" href="#thanks">Remerciements</a></li>
    <li><a data-toggle="pill" href="#javascript">Javascript</a></li>
    <li><a data-toggle="pill" href="#css">CSS</a></li>
</ul>