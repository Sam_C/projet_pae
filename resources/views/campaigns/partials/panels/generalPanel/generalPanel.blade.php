<style>
    .form-group {
        width: 50%;
        padding-left: 5px;
        padding-right: 5px;
        float: left;
    }

    #campaignUUID:hover {
        cursor: default;
    }
</style>
<?php $myuuid = str_replace('-', '', \Ramsey\Uuid\Uuid::uuid1());?>
<div id="general" class="tab-pane fade in active">
    <h3>Paramètres généraux</h3>
    <div class="form-group">
        <label for="campaignName">Nom de la campagne</label>
        <input type="text" class="form-control" id="campaignName" name="campaignName" placeholder="Saisir le nom...">
    </div>
    <div class="form-group">
        <label for="campaignSlug">Slug de la campagne</label>
        <input type="text" class="form-control" id="campaignSlug" name="campaignSlug" placeholder="Saisir le slug...">
    </div>
    <div class="form-group">
        <label for="campaignUUID">UUID de la campagne</label>
        <input type="text" disabled class="form-control" id="campaignUUID" name="campaignUUID" value="{{$myuuid}}">
    </div>
    <div class="form-group">
        <label for="campaignMail">Serveur Mail</label>
        <select class="form-control" id="campaignMail" name="campaignmail">
            <option value="smtp.truc.bidule">Nom du serveur1</option>
            <option value="smtp.truc.bidule">Nom du serveur2</option>
            <option value="smtp.truc.bidule">Nom du serveur3</option>
        </select>
    </div>
    <div class="form-group">
        @include('campaigns.modals.importGuests')
    </div>
</div>