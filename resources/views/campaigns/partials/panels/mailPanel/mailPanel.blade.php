<!-- Mail panel-->
<div id="mail" class="tab-pane fade">
    <!-- sub header -->
    <ul class="nav nav-pills">
        <li><a data-toggle="pill" href="#mail_text">Texte</a></li>
        <li><a data-toggle="pill" href="#mail_html">HTML</a></li>
    </ul>
    <!-- text subpanel-->
    <div class="tab-content">
        <div id="mail_text" class="tab-pane fade">
            <div id="mail_text_editor" class="MyEditor"></div>
        </div>
        <!-- HTML subpanel-->
        <div id="mail_html" class="tab-pane fade">
            <div id="mail_html_editor" class="MyEditor"></div>
        </div>
    </div>
</div>