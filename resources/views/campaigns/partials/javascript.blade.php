<script src="{{asset('js/AreYouSure/jquery.are-you-sure.js')}}" type="text/javascript" charset="utf-8"></script>
<script src="{{asset('admin/ace/src-noconflict/ace.js')}}" type="text/javascript" charset="utf-8"></script>

<script>

    //form leave check :
    $('form').areYouSure();

    //partie mail
    start = (Date.now());
    var mail_text = ace.edit("mail_text_editor");
    mail_text.setTheme("ace/theme/monokai");
    mail_text.getSession().setMode("ace/mode/text");

    var mail_html = ace.edit("mail_html_editor");
    mail_html.setTheme("ace/theme/monokai");
    mail_html.getSession().setMode("ace/mode/html");

    //partie formulaire
    var form_html = ace.edit("form_html_editor");
    form_html.setTheme("ace/theme/monokai");
    form_html.getSession().setMode("ace/mode/html");


    //partie thank
    var thank_html = ace.edit("thank_html_editor");
    thank_html.setTheme("ace/theme/monokai");
    thank_html.getSession().setMode("ace/mode/html");
    thank_html.setValue("<!--do not remove : css begins here -->\n<!--do not remove : css ends here -->");

    var form_js = ace.edit("javascript_editor");
    form_js.setTheme("ace/theme/monokai");
    form_js.getSession().setMode("ace/mode/javascript");

    var thank_css = ace.edit("css_editor");
    thank_css.setTheme("ace/theme/monokai");
    thank_css.getSession().setMode("ace/mode/css");


    function injectCSS() {
        editor = thank_html;
        console.log(editor);
        editor.find('<!--do not remove : css begins here -->');
        editor.replace('<style>\n' + thank_css.getValue() + '</style>');
        console.log(editor.getSession());


    }

    function showHTMLInIFrame() {
        $('#return').html("<iframe style='border: 0;' src=" +
                "data:text/html," + encodeURIComponent(thank_html.getValue()) +
                "></iframe>");
    }
    thank_html.on("input", showHTMLInIFrame);

    //        $('document').ready(function () {
    //
    //        })
</script>