<button type="button" class="badge" data-toggle="modal" data-target="#myModal{{$userrecord->id}}" style="width: 30px">
    ...
</button>

<!-- Modal -->
{{--@if(old('id') == $userrecord->id)--}}
{{--<div id="myModal{{$userrecord->id}}" class="modal fade in" role="dialog" style="display: block">--}}
{{--@else--}}
<div id="myModal{{$userrecord->id}}" class="modal fade" role="dialog">
    {{--@endif--}}
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form action="{{route('admin.users.update')}}" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">editer utilisateur</h4>
                </div>
                <div class="modal-body">
                    @if(old('_method') == "PATCH")
                        @if(old('id') == $userrecord->id)
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        @endif
                    @endif
                    <div class="form-group">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="PATCH">
                        <input type="hidden" id="id" name="id" value="{{$userrecord->id}}">


                        <label for="firstname">Prénom</label>
                        <input id="firstname" name="firstname" class="form-control" type="text"
                               value="{{$userrecord->firstname}}" required>

                        <label for="lastname">Nom</label>
                        <input id="lastname" name="lastname" class="form-control" type="text"
                               value="{{$userrecord->lastname}}" required>

                        <label for="compagy">Société</label>
                        <input id="compagy" name="company" class="form-control" type="text"
                               value="{{$userrecord->company}}">

                        <label for="email">adresse mail (*) </label>
                        <input id="email" name="email" class="form-control" type="text" value="{{$userrecord->email}}">

                        <label for="password">Mot de passe</label>
                        <input id="password" name="password" class="form-control" type="password"
                               value="{{$userrecord->password}}">

                        <label for="level">Niveau de droits</label>

                        <select name="level" id="level" class="form-control">
                            <option value="0" @if($userrecord->level==0) selected @endif>Utilisateur</option>
                            <option value="1" @if($userrecord->level==1) selected @endif>Administrateur</option>
                        </select>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                        <button type="reset" class="btn btn-warning">Réinit.</button>
                        <button type="submit" class="btn btn-danger">OK</button>
                    </div>

                </div>
            </form>
        </div>

    </div>
</div>

