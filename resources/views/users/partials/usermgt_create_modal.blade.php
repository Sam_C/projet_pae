<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalAdd" style="width: 100%">+</button>


<div id="myModalAdd" class="modal fade" role="dialog">
    {{--@endif--}}
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form action="{{route('admin.users.create')}}" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Créer utilisateur</h4>
                </div>
                <div class="modal-body">
                    @if(old('_method')=="POST")
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>

                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach

                            </ul>
                        </div>
                    @endif
                    @endif
                    <div class="form-group">
                        {{csrf_field()}}


                        <label for="firstname">Prénom</label>
                        <input id="firstname" name="firstname" class="form-control" type="text"
                               value="@if(old('_method')=="POST"){{old('firstname')}}@endif" required>

                        <label for="lastname">Nom</label>
                        <input id="lastname" name="lastname" class="form-control" type="text"
                               value="@if(old('_method')=="POST"){{old('lasttname')}}@endif" required>

                        <label for="company">Société</label>
                        <input id="company" name="company" class="form-control" type="text"
                               value="@if(old('_method')=="POST"){{old('company')}}@endif">

                        <label for="email">adresse mail (*) </label>
                        <input id="email" name="email" class="form-control" type="text"
                               value="@if(old('_method')=="POST"){{old('email')}}@endif" required>

                        <label for="password">Mot de passe</label>
                        <input id="password" name="password" class="form-control" type="password"
                               value="@if(old('_method')=="POST"){{old('password')}}@endif" required>

                        <label for="level">Niveau de droits</label>

                        <select name="level" id="level" class="form-control">
                            <option value="0" @if(old('_method')=="POST" )@if(old('level')==1)selected @endif @endif>
                                Utilisateur
                            </option>
                            <option value="1" @if(old('_method')=="POST" )@if(old('level')==0)selected @endif @endif>
                                Administrateur
                            </option>
                        </select>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                        <button type="reset" class="btn btn-warning">Réinit.</button>
                        <button type="submit" class="btn btn-danger">OK</button>
                    </div>


                </div>
            </form>
        </div>

    </div>
</div>

