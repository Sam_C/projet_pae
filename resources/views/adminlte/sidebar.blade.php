
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @include('adminlte.sidebar.userpanel')

        <!-- search form (Optional) -->
        @include('adminlte.sidebar.searchform')
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        @include('adminlte.sidebar.sidebarmenu')
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
