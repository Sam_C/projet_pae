{{--{{dd(route('admin.users.index'))}}--}}
<ul class="sidebar-menu">
    <li class="header">Utilisateurs</li>
    <!-- Optionally, you can add icons to the links -->
    <li class="{{strstr (route('admin.users.index'),request()->getRequestUri())!==false ? "active" : ''}}">
        <a href="{{route('admin.users.index')}}"><i class="fa fa-users"></i> <span>Gérer...</span></a>
    </li>

    <li class="header">Campagnes</li>
    <li class="{{strstr (route('admin.campaigns.create'),request()->getRequestUri())!==false ? "active" : ''}}">
        <a href="{{route('admin.campaigns.create')}}"><i class="fa fa-pencil-square-o"></i> <span>Créer</span></a>
    </li>

    <li class="header">Champs personnalisés</li>
    <li class="{{strstr (route('customfield.index'),request()->getRequestUri())!==false ? "active" : ''}}">
        <a href="{{route('customfield.index')}}"><i class="fa fa-asterisk"></i> <span>Lister</span></a>
    </li>
    <li class="{{strstr (route('customfield.formBuilder'),request()->getRequestUri())!==false ? "active" : ''}}">
        <a href="{{route('customfield.formBuilder')}}"><i class="fa fa-magic"></i> <span>Créer</span></a>
    </li>

    {{--<li class="treeview">--}}
    {{--<a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>--}}
    {{--<span class="pull-right-container">--}}
    {{--<i class="fa fa-angle-left pull-right"></i>--}}
    {{--</span>--}}
    {{--</a>--}}
    {{--<ul class="treeview-menu">--}}
    {{--<li class=""><a href="#">Link in level 2</a></li>--}}
    {{--<li><a href="#">Link in level 2</a></li>--}}
    {{--</ul>--}}
    {{--</li>--}}
</ul>