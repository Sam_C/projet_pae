<div class="user-panel">
    <div class="pull-left image">
        <img src="{{asset('admin/adminlte/dist/img/user-icon.png')}}" class="img-circle" alt="User Image">
    </div>
    <div class="pull-left info">
        @yield('user-sidebar-infos')
    </div>
</div>
