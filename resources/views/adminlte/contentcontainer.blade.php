<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        @yield('content-header')
    </section>

    <!-- Main content -->
    <section class="content">

        @yield('content-body')<!-- Your Page Content Here -->

    </section>
    <!-- /.content -->
</div>
