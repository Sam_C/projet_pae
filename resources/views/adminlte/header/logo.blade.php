<a href="admin" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini">@yield('logo-mini-caption')</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg">@yield('logo-maxi-caption')</span>
</a>