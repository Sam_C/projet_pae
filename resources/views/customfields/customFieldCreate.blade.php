@extends('layouts.adminlte')
@include('adminlte_static_content')

<style type="text/css" media="screen">
    .MyEditorContainer {
        position: relative;
        height: 60%;
        min-height: 60%;
    }

    .MyEditor {
        position: absolute;
        height: 90%;
        min-height: 100%;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        /*height: 480px;*/
        /*width: 100%;*/
        /*bottom: 0;*/
    }
</style>


@section('content-header')
    <h1>un titre !</h1>


@endsection
@section('content-body')

    Menu création champs persos.
    <div class="content">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>

                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach

                </ul>
            </div>
        @endif

        <form action="" method="post" onsubmit="$('#formHTML').val(stream_html_editor.getValue());">
            {{csrf_field()}}
            <div class="jumbotron">
                <div class="control-group {{$errors->has('streamName') ? 'has-error' : ''}}">
                    <label for="streamName">Nom du champ perso :</label>
                    <input type="text" id="streamName" name="streamName" class="form-control" onkeyup="toSlug(this)"
                           onchange="toSlug(this)" value="{{old('streamName')}}">
                </div>

                <div class="control-group {{$errors->has('streamName') ? 'has-error' : ''}}">
                    <label for="streamlabel">Etiquette du champ perso :</label>
                    <input type="text" id="streamLabel" name="streamLabel" class="form-control"
                           value="{{old('streamLabel')}}">
                </div>

                <div id="streamSlugFields" class="control-group {{$errors->has('streamSlug') ? 'has-error' : ''}}">

                    <label for="streamSlug">Slug du champ perso
                        <button type="button" class="btn btn-secondary badge" onclick="checkSlug(this)" id="dispo">dispo
                            ?
                        </button>
                        :</label>
                    <input type="text" id="streamSlug" name="streamSlug" class="form-control"
                           value="{{old('streamSlug')}}">
                </div>
                {{--<div class="control-group {{$errors->has('streamLabel') ? 'has-error' : ''}}">--}}
                {{--<label for="streamLabel">Label du champ perso : </label>--}}
                {{--<input type="text" id="streamLabel" name="streamLabel" class="form-control"--}}
                {{--value="{{old('streamLabel')}}"></div>--}}
                <div class="control-group {{$errors->has('streamValidationRule') ? 'has-error' : ''}}">
                    <label for="streamValidationRule">Règle de validation du champ perso :</label>
                    <input type="text" id="streamValidationRule" name="streamValidationRule" class="form-control"
                           value="{{old('ValidationRule')}}">

                    @include('customfields.partials.validationRuleBuilder')

                </div>
                <label for="formHTML"></label>
                <textarea id="formHTML" name="formHTML" style="display: none"></textarea>
            </div>
            <div class="MyFixedValues">
                <ul class="list-group has-info">
                    <li class="list-group-item list-group-item-info">@id@ sera remplacé par un id et un name
                        automatiques, pour le label également, s'il est renseigné.
                    </li>
                    <li class="list-group-item list-group-item-info">@old-value@ indique où seront injectées les
                        anciennes valeurs si le formulaire échoue
                    </li>
                    <li class="list-group-item list-group-item-info">@error-class@ indique où la classe "has-error" sera
                        injectée en cas d'échec du formulaire.
                    </li>
                </ul>
            </div>
            <div class="MyEditorContainer">
                <div id="editor" class="MyEditor">{{old('formHTML')}}</div>
            </div>
            <div>
                <button type="submit" class="btn btn-info">save</button>
            </div>
        </form>
    </div>

    <div id="preview" onChange="renderPreview()">

    </div>

    <button type="button" onclick="renderPreview()">prévisualiser</button>



@endsection
@include('customfields.partials.js')