@extends('layouts.adminlte')
@include('adminlte_static_content')


@section('content-header')
    <h1>un titre !</h1>


@endsection

@section('content-body')

    @if (Session::get('success'))

        <div class="alert alert-success">
            <ul>

                <li>{{ Session::get('success') }}</li>

            </ul>
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif




    <table class="table table-responsive">
        <thead>
        <tr>
            <th></th>
            <th>name</th>
            <th>slug</th>
            <th>render</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $field)
            <tr>
                {{--{{dd($field)}}--}}
                {{--<td>{{$field['items']->name}}</td>--}}
                <td>@include('customfields.modals.deletestream')</td>
                <td><a href="{{route('customfield.formBuilder',$field['item']->slug)}}"> {{($field['item']->name)}}</a>
                </td>
                <td>{{($field['item']->slug)}}</td>
                <td>{!! $field['rendered'] !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>






@endsection
@section('aditionnal-scripts')

@endsection