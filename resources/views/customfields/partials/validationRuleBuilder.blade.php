<style>
    .ruleSection {
        border-style: solid;
        border-width: 1px;
        margin-top: 10px;
        padding-bottom: 10px;

    }
</style>
<button type="button" data-toggle="collapse" data-target="#validationBuilder" class="btn btn-info">Constructeur de
    règle...
</button>

<div id="validationBuilder" class="collapse"> <!-- classe = collapse -->
    <div class="row">
        <div id="dataTypeDiv" class="col-sm-12 col-lg-6">
            <label for="dataType">Type de données</label>
            <select id="dataType" class="form-control" onchange="buildValidation(this)">
                <option selected value="">Type...</option>
                <option value="string">Chaine de caractères</option>
                <option value="email">email</option>
                <option value="ip">adresse IP</option>
                <option value="ipv4">adresse IP v4</option>
                <option value="ipv6">adresse IP v6</option>
                <option value="date">Date</option>
                <option value="integer">Valeur numérique entière</option>
                <option value="numeric">Valeur numérique réelle</option>
                <option value="boolean">Oui/Non</option>
                <option value="image">Image</option>
                <option value="file">Fichier</option>

            </select>
        </div>
        <div id="dataRequiredDiv" class="col-sm-12 col-lg-6">
            <label for="dataRequired">Requis</label>
            <select id="dataRequired" class="form-control" onchange="buildValidation(this)">
                <option value="required">Oui</option>
                <option value="" selected>Non</option>
                <option value="nullable">Nullable</option>
                <option value="filled">Doit contenir une valeur</option>
            </select>
        </div>
    </div>


    <div id="constraints" class="row">
        <div id="stringConstraintsDiv" class="col-sm-12 col-lg-12 ruleSection" style="display: none">
            <div id="stringAlphaNumDiv">
                <label for="stringConstraints">Contraintes de chaine</label>
                <select id="stringConstraints" class="form-control" onchange="buildValidation(this)">
                    <option selected value="">Format libre</option>
                    <option value="alpha">Format alphabétique</option>
                    <option value="alpha_num">Format alphanumérique</option>
                    <option value="alpha_dash">Format alphanumérique et traits ("-","_")</option>
                </select>
            </div>
            <div id="stringMinAndMaxLengthDiv">
                <label for="minLength">Longueur (si chaine) ou valeur mini : </label>
                <input id="minLength" class="form-control" type="number" min="0" step="1"
                       onchange="buildValidation(this)">
                <label for="maxLength">Longueur (si chaine) ou valeur maxi : </label>
                <input id="maxLength" class="form-control" type="number" min="0" step="1"
                       onchange="buildValidation(this)">
            </div>
            {{--correspond à la règle size--}}
            <div id="fixedLengthDiv">
                <label for="fixedLength">taille fixe : </label>
                <input id="fixedLength" class="form-control" type="number" min="0" step="1"
                       onchange="buildValidation(this)">
            </div>
        </div>

        <div id="imageDimensionDiv" class="col-sm-12 col-lg-12 ruleSection" style="display: none">
            <div id="imageDimensionMinHeightDiv">
                <label for="imageDimensionMinHeight">Hauteur mini : </label>
                <input id="imageDimensionMinHeight" class="form-control" type="number" min="0" step="100"
                       onchange="buildValidation(this)">
            </div>
            <div id="imageDimensionMaxHeightDiv">
                <label for="imageDimensionMaxHeight">Hauteur maxi : </label>
                <input id="imageDimensionMaxHeight" class="form-control" type="number" min="0" step="100"
                       onchange="buildValidation(this)">
            </div>
            <div id="imageDimensionMinWidthDiv">
                <label for="imageDimensionMinWidth">Largeur mini : </label>
                <input id="imageDimensionMinWidth" class="form-control" type="number" min="0" step="100"
                       onchange="buildValidation(this)">
            </div>
            <div id="imageDimensionMaxWidthDiv">
                <label for="imageDimensionMaxWidth">Largeur maxi : </label>
                <input id="imageDimensionMaxWidth" class="form-control" type="number" min="0" step="100"
                       onchange="buildValidation(this)">
            </div>

            <div id="imageDimensionRatioDiv"><label for="imageDimensionRatio">Ratio </label>
                <input id="imageDimensionRatio" class="form-control" type="text" onchange="buildValidation(this)"></div>
        </div>

        <div class="col-lg-12 col-sm-12 ruleSection" id="fileTypeDiv" style="display: none">
            <div id="mimeTypesDiv" class="col-lg-6 col-sm-6">
                <label for="mimeTypes">Types MIME séparés par des virgules</label>

                <input id="mimeTypes" type="text" class="form-control"
                       placeholder="video/avi,video/mpeg,video/quicktime"
                       onchange="buildValidation(this)">
                <span>Une liste complète des types mimes <a target="_blank"
                                                            href="https://developer.mozilla.org/fr/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types">les
                        plus fréquents</a></span>
            </div>
            <div id="fileExtDiv" class="col-lg-6 col-sm-6">
                <label for="fileExt">Extentions de fichiers, séparées par des virgules</label>
                {{--<br/><span>Une liste complète des types mimes <a target="_blank" href="https://developer.mozilla.org/fr/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types">les plus fréquents</a></span>--}}
                <input id="fileExt" type="text" class="form-control" placeholder="jpeg,bmp,png"
                       onchange="buildValidation(this)">
            </div>
            <div id="fileSizeDiv" class="col-lg-6 col-sm-6">
                <label for="fileSize">Taille du fichier (KB) : </label>
                <input id="fileSize" class="form-control" type="number" min="0" step="50"
                       onchange="buildValidation(this)">
            </div>
        </div>

    </div>

    <div id="validationPreview" class="form-group">
        <label for="validationRulePrev">Prévisualisation de la règle de validation :</label>
        <input id="validationRulePrev" value="" class="form-control">
        <button type="button" onclick="useRule()" data-toggle="collapse" data-target="#validationBuilder">utiliser cette
            règle.
        </button>
    </div>


    <script>


        //TODO : implement date logic
        //this is the object we'll use to build the rule,
        //we will append constraints objects to it depending on the type of field
        var val = {
            mainRule: null,
            constraints: {}
        };

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // below come the constraints objects
        // if they have properties prefixed by an underscore, they are meant to be used internally only,
        // this is for special syntaxes such as images dimensions, as a validation usualy comes this form :
        // keyword:value
        // and in some special cases :
        // keyword:subkeyword1=value1,subkeyword2=value2, and so on...
        //
        // names are explicit enough to be uncommented.
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var fieldConstraint = {
            required: function () {
                return $('#dataRequired').val()
            },
        };

        var stringConstraints = {
            min: function () {
                myVal = $('#minLength').val();
                return myVal !== "" ? 'min:' + myVal : "";
            },
            max: function () {
                myVal = $('#maxLength').val();
                return myVal !== "" ? 'max:' + myVal : "";
            },
            size: function () {
                myVal = $('#fixedLength').val();
                return myVal !== "" ? 'size:' + myVal : "";
            },
            type: function () {
                myVal = $('#stringConstraints').val();
                return myVal !== null ? myVal : "";
            },
        };

        var mailConstraints = {
            min: function () {
                myVal = $('#minLength').val();
                return myVal !== "" ? 'min:' + myVal : "";
            },
            max: function () {
                myVal = $('#maxLength').val();
                return myVal !== "" ? 'max:' + myVal : "";
            },
        };

        var fileConstraints = {
            size: function () {
                myVal = $('#fixedLength').val();
                return myVal !== "" ? 'size:' + myVal : "";
            },
            mimeTypefunction(){
                myVal = $('#mimeTypes').val();
                return myVal !== "" ? 'mimetypes:' + myVal : "";
            },
            fileExt: function () {
                myVal = $('#fileExt').val();
                return myVal !== "" ? 'mimes:' + myVal : "";
            },
        };

        var imageConstraints = {
            _mimeType: function () {
                myVal = $('#mimeTypes').val();
                return myVal !== "" ? 'mimetypes:' + myVal : "";
            },
            _fileExt: function () {
                myVal = $('#fileExt').val();
                return myVal !== "" ? 'mimes:' + myVal : "";
            },
            _minWidth: function () {
                myVal = $('#imageDimensionMinWidth').val();
                return myVal > 0 ? 'min_width=' + myVal : "";
            },
            _minHeight: function () {
                myVal = $('#imageDimensionMinHeight').val();
                return myVal > 0 ? 'min_height=' + myVal : "";
            },
            _maxWidth: function () {
                myVal = $('#imageDimensionMaxWidth').val();
                return myVal > 0 ? 'max_width=' + myVal : "";
            },
            _maxHeight: function () {
                myVal = $('#imageDimensionMaxHeight').val();
                return myVal > 0 ? 'max_height=' + myVal : "";
            },
            _ratio: function () {
                myVal = $('#imageDimensionRatio').val();
                return myVal !== "" ? 'ratio=' + myVal : "";
            },
            getAll: function () {
                var tmp = [];
                this._ratio() !== "" ? tmp.push(this._ratio()) : null;
                this._maxHeight() !== "" ? tmp.push(this._maxHeight()) : null;
                this._minHeight() !== "" ? tmp.push(this._minHeight()) : null;
                this._minWidth() !== "" ? tmp.push(this._minWidth()) : null;
                this._maxWidth() !== "" ? tmp.push(this._maxWidth()) : null;


                if (tmp.length > 0) {
                    str = tmp.concat();
                    str = 'dimensions:' + str;
                } else str = "";

                return str;

            }

        };


        function buildValidation(sender) {
            resetElements(sender);

            val.mainRule = $('#dataType').val();
            //reinit constraints before checking the kind of field we're dealing with
            val.constraints = {};
            //dump constraints common to all fields
            fieldConstraint.required != null ? jQuery.extend(val.constraints, fieldConstraint) : null;

            //dump per use constraints
            val.mainRule == 'string' ? jQuery.extend(val.constraints, stringConstraints) : null;
            val.mainRule == 'email' ? jQuery.extend(val.constraints, mailConstraints) : null;
            val.mainRule == 'file' ? jQuery.extend(val.constraints, fileConstraints) : null;
            val.mainRule == 'image' ? jQuery.extend(val.constraints, imageConstraints) : null;

            $('#validationRulePrev').val(val.mainRule);

            //we append sub objects into the main object's (val) property (constraints)
            for (var k in val.constraints) {
                if (val.constraints[k]() !== "") {
                    if (k.charAt(0) !== '_') //let's assume underscore prefix means private property...
                        $('#validationRulePrev').val($('#validationRulePrev').val() + '|' + val.constraints[k]());
                }
            }
        }

        function resetElements(sender) {
            //a list of all the fields, so we can go through it,
            //depending on the lists below, if there's a match, they will be shown, else, hidden.


            if (sender.id == 'dataType') {

                myMainMenu = $('#dataType');
                $('#fileSizeDiv').toggle(myMainMenu.val() == 'file');
                $('#imageDimensionDiv').toggle(myMainMenu.val() == 'image');
                $('#fileTypeDiv').toggle(myMainMenu.val() == 'image' || myMainMenu.val() == 'file');

//                $('#fileExtDiv').hide(myMainMenu.val() !== 'image' || myMainMenu.val() !== 'file');

                $('#stringConstraintsDiv').toggle(myMainMenu.val() == 'string' || myMainMenu.val() == 'email' || myMainMenu.val() == 'integer' || myMainMenu.val() == 'numeric');
                //case <> of string (email, integer, etc...), we restrict the length or the value, only :
                $('#fixedLengthDiv').toggle(myMainMenu.val() !== 'email' && myMainMenu.val() !== 'integer' && myMainMenu.val() !== 'numeric');
                $('#stringAlphaNumDiv').toggle(myMainMenu.val() !== 'email' && myMainMenu.val() !== 'integer' && myMainMenu.val() !== 'numeric');

                if (myMainMenu.val() == 'string') {
                    //$('#imageDimensionDiv').hide(true);
                }
            }
        }

        function useRule() {
            $('#streamValidationRule').val($('#validationRulePrev').val());

        }
    </script>

</div>