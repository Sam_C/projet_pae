@section('aditionnal-scripts')

    <script src="{{asset('admin/ace/src-noconflict/ace.js')}}" type="text/javascript" charset="utf-8"></script>

    <script>
        //editeur de texte :
        var stream_html_editor = ace.edit("editor");
        stream_html_editor.setTheme("ace/theme/monokai");
        stream_html_editor.getSession().setMode("ace/mode/php");
        stream_html_editor.$blockScrolling = Infinity;
        stream_html_editor.getSession().on('change', function () {
            renderPreview();
        });

        // if the place is empty, we inject a default HTML tempate....
        {{--if (stream_html_editor.getValue().trim() == "") {--}}
        {{--default_content =  "<div class=\"form-group @has-error@\">\r\n"--}}
        {{--+"\t<label for=\"@slug@\"></label>\r\n"--}}
        {{--+"\t<input type=\"\" id=\"@slug@\" name=\"@slug@\" class=\"form-control\" placeholder=\"\" value=\"@value@\">\r\n"--}}
        {{--+"</div>\r\n";--}}


        {{--stream_html_editor.setValue(default_content);--}}
        {{--}--}}

        function renderPreview() {
            previewField = $('#previewFrame');
            //hide the iframe if visible...
//            previewField.hide(true);
            //request a preview of the current form
            myAjax = $.ajax({
                url: "{{route('customfield.ajaxRender')}}",
                method: "POST",
                data: {
                    '_token': '{{csrf_token()}}',
                    'forceRenderData': stream_html_editor.getValue(), //providing the current HTML
                    'slug': $('#streamSlug').val(), //the current slug
                    'label': $('#streamLabel').val() // and the current label.
                },
//                                dataType: ""
            });

            myAjax.success(function (msg) {
                //let's display raw HTML in the emptied div.
                $('#preview').html(msg);
                console.log(msg);
            });

            myAjax.done(function (msg) {

            });

            myAjax.fail(function (jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });


        }

        //slug :
        function slugify(text) {
            return text.toString().toLowerCase()
                    .replace(/\s+/g, '-')           // Replace spaces with -
                    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
                    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
                    .replace(/^-+/, '')             // Trim - from start of text
                    .replace(/-+$/, '');            // Trim - from end of text
        }


        //dynamically constructs the slug field
        function toSlug(object) {
            //TODO : check if slug already exists...
            console.log(object.value);
            $('#streamSlug').val(slugify(object.value));
        }

        ////
        function checkSlug(sender) {
            console.log($('#streamSlug').val());
            if ($('#streamSlug').val() !== "") {
                myAjaxCheckSlug = $.ajax({
                    url: "{{route('customfield.ajaxCheckSlugIsUnique')}}/" + $('#streamSlug').val(),
                    method: "GET",
                    data: '',
                    dataType: ""
                });

                myAjaxCheckSlug.success(function (msg) {
                    $('#streamSlugFields').removeClass('has-error');
                    $('#streamSlugFields').removeClass('has-success');
                    if (msg > 0) {
                        $('#streamSlugFields').addClass('has-warning');
                        document.getElementById("dispo").style["background-color"] = "orange";
                        //sender.className = 'btn btn-danger badge';
                    } else {
                        $('#streamSlugFields').addClass('has-success');
                        //sender.className = 'btn btn-success badge';
                        document.getElementById("dispo").style["background-color"] = "#3c763d";
                        //sender.attribute.style['background-color'] = "blue";

                    }
                });

                myAjaxCheckSlug.done(function (msg) {
                    //console.log(msg);
                });

                myAjaxCheckSlug.fail(function (jqXHR, textStatus) {
                    alert("Request failed: " + textStatus);
                });
            }
        }
        ;


    </script>
@endsection