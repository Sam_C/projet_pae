<button type="button" class="badge" data-toggle="modal" data-target="#myModalDelete{{($field['item']->slug)}}"
        style="width: 30px;background-color: darkred">x
</button>

<!-- Modal -->
<div id="myModalDelete{{($field['item']->slug)}}" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form action="{{route('customfield.delete')}}" method="post">
                {{csrf_field()}}
                <input type="hidden" id="slug" name="slug" value="{{$field['item']->slug }}">
                <input name="_method" type="hidden" value="DELETE">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Suppression</h4>
                </div>
                <div class="modal-body">
                    Confirmer la suppression ?
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Non</button>
                        <button type="submit" class="btn btn-danger">OUI</button>
                    </div>

                </div>
            </form>
        </div>

    </div>
</div>