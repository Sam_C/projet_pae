<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->string('uuid')->nullable();
            $table->text('txt_mail')->nullable();
            $table->text('html_mail')->nullable();
            $table->text('html_form')->nullable();
            $table->text('html_thank')->nullable();
            $table->text('css')->nullable();
            $table->text('js')->nullable();
            $table->integer('mail_id')->nullable();
            $table->integer('actual_creation_step')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
