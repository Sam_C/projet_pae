<?php

namespace App\Http\Controllers\Imports;

use App\Guest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Excel;
use Ramsey\Uuid\Uuid;

class CsvImportController extends Controller
{
    //

    /**
     * @param $campaignUUID
     *          we'll inject guests in DB using a specific campaign
     * @return a form for basic campaign informations.
     */
    public function form($campaignUUID)
    {
        if (isset($campaignUUID)) {
            return view('imports.partials.fileUpload', ['uuid' => $campaignUUID]);
        } else
            return view('imports.testFileUpload');
    }


    /**
     * @param Request $request
     *              gets an uploaded file, either a CSV, XLS, XLSX one, and parses it.
     * @return a form containing all the fields so we can bind them (or not) to the database.
     *
     * TODO : what happens if we want to upload several guests lists ? Do we keep the files uploaded ?
     */
    public function upload(Request $request)
    {
        //get the file
        $file = $request->file('file');

        //if a campaign is provided,
        // we name the file : campaignUUID.originalExtention
        //else, we keep the original filename
        if ($request->request->has('campaignUUID')) {
            $XLfilename = $request->request->filter('campaignUUID') . '.' . $file->getClientOriginalExtension();
//            dd(file_exists($XLfilename));
            //TODO : do not overwrite existing data ?
        } else
            $XLfilename = $file->getClientOriginalName();

        //then we store it.
        $file->move('storage', $XLfilename);

        //we load the file
        $xl = \Excel::load('public/storage/' . $XLfilename);
        //and convert it to array
        $fields = ($xl->sheet(0)->toArray());

        //suppose the first row contains heading of the table, we isolate it to send it to the view
        $data['headers'] = $fields[0];
        // we generate a preview of the data, sending a few records in a different array
        // NOTE : we can use XLS and XLSX data quite transparently, but we'll need to split CSV rows

        $max = (sizeof($fields) < 6) ? sizeof($fields) : 6;
        if ($file->getClientOriginalExtension() !== 'csv') {
            for ($i = 1; $i < $max || $i < 5; $i++) {
                $data["rows"][] = $fields[$i];
            }
        } else {

            for ($i = 1; $i < $max || $i < 5; $i++) {
//                dd($fields[$i][0]);

                $data["rows"][] = str_getcsv($fields[$i][0], ';');
            }
            //we set as many empty headers as they are columns

            $data['headers'] = array_fill(0, sizeof(str_getcsv($fields[$i][0], ';')), "");

        }
        //we return the stored filename to the view, so we can use it in the form for the next step
        $data["file"] = 'public/storage/' . $XLfilename;
        //we return a specific view : first case, we are provided a campaign's UUID, so we'll have to use it
        //second case, we import a bulk guest list.
        if ($request->request->has('campaignUUID')) {
            $data['uuid'] = $request->request->filter('campaignUUID');
            return view('imports.partials.previewImport', ['data' => $data]);
        } else
//        dump($data);
            return view('imports.previewImportTable', ['data' => $data]);
    }

    /**
     * @param Request $request
     *          the request sent by the form is forged by some parameters filled by $this->upload,
     *          such as filename or campaign's UUID
     *
     *          We are going to treat the previously uploaded file, so we can bind some rows we gonna use directly to
     *          table's fields, serialize other fields we may use, ignore the fields that we do not require.
     *
     *          User is provided a list of fields (lastname, firstname, etc...) and is able to input his own fields' names
     * @return string
     */
    public function link(Request $request)
    {

        //these are the fields from the table
        $protected_fieldnames = [
            'firstname', 'lastname', 'tel', 'email', 'company', 'extra_data'
        ];

        //enumerate submitted fields, put their values into an array  :
        $i = 0;
        $fieldlist = array();
        $currentFieldName = 'field' . $i;
        while ($request->request->has($currentFieldName) == true) {

            $fieldlist[] = camel_case($request->request->filter($currentFieldName));
            $i++;
            $currentFieldName = 'field' . $i;
        }

        //TODO : continue bindings....

        //reload the uploaded file
        $xl = \Excel::load($request->request->filter('list_name'));
        //and convert it to array
        $xldata = $xl->sheet(0)->toArray();

        //keep the data in an array, meaning we skip the very first row who's supposed to be headers
        $recList = array();
        for ($i = 1; $i < sizeof($xldata); $i++) {

            //we make an array for each row of the file, we still have to split CSV values, but can use XLS values as is.
            $rec = array();
            //case we work on something different of a comma separated values fields
            //in both cases, we output an associative array "column name" => value
            if ((substr($request->request->filter('list_name'), sizeof($request->request->filter('list_name')) - 4)) !== 'csv') {
                foreach ($xldata[$i] as $index => $value) {
                    if ($fieldlist[$index] !== null && $fieldlist[$index] !== "") {

                        $rec[$fieldlist[$index]] = $value;
                    }

                }
            } else {
                foreach (str_getcsv($xldata[$i][0], ';') as $index => $value) {
                    if ($fieldlist[$index] !== null && $fieldlist[$index] !== "") {

                        $rec[$fieldlist[$index]] = $value;
                    }
                }
            }
            //we append this row to the list of rows.
            $recList[] = $rec;
        }

        //we gonna run through the $recList above and see if column name matches db fields
        //if so, we map the column to the field, if not, we serialize it as a pair : columnName|value
        // and append it to the already existing serialized values if any
        $finalRecList = array();

        foreach ($recList as $rec) {
            $finalRec = array();
            $serialize = "";
            foreach ($rec as $k => $v) {
                //we got a match to static dbField
                if (array_search($k, $protected_fieldnames) !== false) {
                    $finalRec[$k] = $v;
                } //else we unserialize the extra_data field, append this value, and serialize again
                else {
                    $serialize = serialize(unserialize($serialize)[] = [$k => $v]);
                }
                $finalRec['extra_data'] = $serialize;
            }
            //we append it to a list of records.
            $finalRecList[] = $finalRec;
        }
        //final step, we go through $finalRecList[] and try to insert it in db
        $reportObject = array();
        $reportObjectList = array();
        foreach ($finalRecList as $rec) {
            $guest = new Guest;
            foreach ($rec as $k => $v) {
                //if any of the column matches a dbFieldName, we bind them
                if ($k == 'firstname') $guest->firstname = $v;
                if ($k == 'lastname') $guest->lastname = $v;
                if ($k == 'email') $guest->email = $v;
                if ($k == 'tel') $guest->tel = $v;
                if ($k == 'company') $guest->company = $v;
                //we do so with serialized datas
                if ($k == 'extra_data') $guest->extra_data = $v;
                //and we generate an UUID per guest
                $guest->uuid = str_replace('-', '', Uuid::uuid1()->toString());

            }
            //case we are provided a campaign's UUID, we insert it too.
            if ($request->request->has('campaignUUID'))
                $guest->campaign_uuid = $request->request->filter('campaignUUID');


            //$reportObjectList[] could be sent to a view...
            $reportObject['guest'] = $guest; //the Collection Object
            $reportObject['status'] = $guest->save(); //the result of the insertion
            $reportObjectList[] = $reportObject; //both from above in an associative array


            //case we come from a campaign form, we just return that treatment is over...
            if ($request->request->has('campaignUUID'))
                return "Fait";
        }
    }
}
