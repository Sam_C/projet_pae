<?php

namespace App\Http\Controllers\UserMgt;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserMgtController extends Controller
{
    //

    public function index()
    {
        $data = User::paginate(15);
        return view('users.userindex', ['data' => $data]);
    }

    public function delete(Request $request){
        //dump($request);
        $usr = User::find($request->request->filter('id'));

        if ($usr != null){
            if ($usr->delete()==true)
                return redirect()->back()->with('success', 'Utilisateur effacé');
        }
        else
            return redirect()->back()->with('errors', 'erreur lors de la suppression');




    }

    public function create(Request $request){
        //dd($request);
        $this->validate($request, [
            'firstname' => 'required|string|min:2|max:255',
            'lastname' => 'required|string|min:2|max:255',
            'company' => 'string|min:1|max:255',
            'email' => 'required|email|max:255',
        ]);

        $usr = new User;
        $usr->firstname = $request->request->filter('firstname');
        $usr->lastname = $request->request->filter('lastname');
        $usr->company = $request->request->filter('company');
        $usr->email = $request->request->filter('email');
        $usr->password = $request->request->filter('password');
        $usr->level = $request->request->filter('level');
        $usr->save();
        //
        return redirect()->back()->with('success', 'Utilisateur créé');

    }

    public function update(Request $request){
//        dump($request);
        $this->validate($request,[
            'firstname' => 'required|string|min:2|max:255',
            'lastname'  => 'required|string|min:2|max:255',
            'company'   => 'string|min:1|max:255',
            'email'     => 'required|email|max:255',
        ]);

        $usr = User::find($request->request->filter('id'));
        $usr->firstname = $request->request->filter('firstname');
        $usr->lastname = $request->request->filter('lastname');
        $usr->company = $request->request->filter('company');
        $usr->email = $request->request->filter('email');
        $usr->password = $request->request->filter('password');
        $usr->level = $request->request->filter('level');
        $usr->save();
        return redirect()->back()->with('success', 'Utilisateur mis à jour');
    }

    public function fakeUsers(){
        $fk = \Faker\Factory::create();
        //dump($fk);
        for ($i=0;$i<15;$i++){
            $usr = new User();
            $usr->firstname = $fk->firstName;
            $usr->lastname=$fk->lastName;
            $usr->company = $fk->company;
            $usr->password= bcrypt($fk->password);
            $usr->email    = $fk->unique()->email;
            $usr->level    = ($i % 5 == 0) ? 1  :   0;
            dump($usr);
            dump($usr->save());

        }
        return redirect(route('admin.users.index'));
    }

}
