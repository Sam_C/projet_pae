<?php

namespace App\Http\Controllers\Forms;

use App\Campaign;
use App\Http\Controllers\Streams\StreamV2Controller;
use App\Stream;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FormController extends Controller
{
    //

    public function index($slug = 'campagne-test')
    {
        $form = Campaign::all()->where('slug', '=', $slug)->first();

        //todo : check if result  > 0

        $form = $form->html_form;

        $form = explode('$', $form);

        $html = '';
        $streams = Stream::all();
        foreach ($form as $index => $element) {
            //special case, token
            if ($element == 'token')
                $form[$index] = csrf_field();
            foreach ($streams as $customfield) {
                if ($element == $customfield->slug) {
                    $renderer = new StreamV2Controller;
                    //special case, the honeypot, we put encrypted timestamp as value...

                    if ($element == 'honeypot' || $element == 'token') {

                        if ($element == 'honeypot')
                            $form[$index] = $renderer->render(['slug' => $customfield->slug, 'value' => encrypt(time())]);


                    } else {

                        $form[$index] = $renderer->render(['slug' => $customfield->slug]);
                    }
                } else {
                    //we put the separator back :
                    $form[$index] = (sizeof(trim($form[$index])) > 1) ? '$' . $form[$index] : $form[$index];
                }
            }
        }

        $html = implode('', $form);

        return $html;
    }

}
