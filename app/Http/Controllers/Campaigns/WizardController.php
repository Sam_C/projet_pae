<?php

namespace App\Http\Controllers\Campaigns;

use App\Campaign;
use App\Stream;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class WizardController extends Controller
{
    //
    public function step1()
    {

        $uuid = str_replace('-', '', Uuid::uuid1());

        return view('campaigns.wizard.step1_name_slug_etc', ['uuid' => $uuid]);

    }

    public function step2(Request $request)
    {
        //dump($request);
        $test = Campaign::all()->where('uuid', '=', $request->request->filter('UUID'))->first();
        $test == null ? $campaign = new Campaign() : $campaign = $test;
        $campaign->uuid = $request->request->filter("UUID");
        $campaign->name = $request->request->filter("name");
        $campaign->slug = $request->request->filter("slug");
        $campaign->save();
        return view('campaigns.wizard.step2_guests', ['uuid' => $request->request->filter("UUID")]);

    }

    public function step3(Request $request)
    {
//        dump($request);
        $campaign = Campaign::all()->where('uuid', '=', $request->request->filter('UUID'))->first();
        $customfields = Stream::where('slug', 'not like', '%' . $campaign->slug . '%')->get();
//        where('slug','like','%'.$request->request->filter('UUID').'%');
//        dd($customfields);
        $campaign_customfields = Stream::where('slug', 'like', '%_' . $campaign->slug . '%')->get();
        return view('campaigns.wizard.step3_customfields', ['uuid' => $request->request->filter('UUID'), 'fields' => $customfields, 'campaignFields' => $campaign_customfields]);

    }

    public function step4(Request $request)
    {
        //dump($request);

        $basket = array();

        $i = 0;
        while ($i < Stream::all()->count()) {
            if ($request->request->has("field$i"))
                $basket[] = Stream::all()->where('slug', '=', $request->request->filter("field$i"))->first();
            $i++;
        }
        //dump($basket);

        //we get the campaign slug :
        $campaignSlug = Campaign::all()->where('uuid', '=', $request->request->filter('UUID'))->first()->slug;
        foreach ($basket as $i => $field) {
            //check if the same field already exists... if not, we duplicate the original,
            // appending the campaign's slug to it's own

            //let's build a new slug :
            $newSlug = $field->slug . '_' . ($campaignSlug);
            $current = Stream::all()->where('slug', '=', $newSlug);
            dump($current->count());
            
//            dump($current->count());
//            if ($current->count() ==0) {
//                $current = $current->first();
//                $current = Stream::find($field->id);
//                $current = $current->replicate();
//                $current->slug = $newSlug;
//                $current->save();


//            }
            //we swap, in the basket, to the customfield dedicated to this campaign
//            $basket[$i] = Stream::all()->where('slug','=',$newSlug)->first();
        }

        //we return a view with the fresh customfields, in order to customize them


    }
}
