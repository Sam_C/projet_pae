<?php

namespace App\Http\Controllers\Streams;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Stream;
use App\Http\Controllers\Streams\StreamController;

class CustomFieldsController extends Controller
{
    //
    public function createCustomFieldForm()
    {

        return view('customfield.customFieldCreate');
    }

    public function store(Request $request)
    {
        dump($request);

        $this->validate($request, [
            'streamName' => 'required|string|min:3',
            'streamSlug' => 'string|min:3|unique:streams,slug',
            '' => '',
        ],
            [
                'streamName.required' => 'Le nom du champs personnalisé doit être renseigné',
                'streamSlug.unique' => 'Le slug du champs personnalisé doit être unique',
            ]
        );

        $str = new Stream;
        $str->name = $request->request->filter('streamName');
        $str->slug = $request->request->filter('streamSlug');
        $str->label = $request->request->filter('streamLabel');
        $str->validator = $request->request->filter('streamName');
        $str->render = $request->request->filter('formHTML');
        $str->save();

    }

    public function ajaxCheckSlugIsUnique($slug)
    {
        if ($slug == null || $slug == "") {
            return json_encode(1);
        } else {
            $res = Stream::all()->where('slug', '=', $slug)->count();
            return (json_encode($res));
        }

    }

    public function index()
    {

        $liste = Stream::all();
        $rendus = array();

        $renderer = new StreamController;

        foreach ($liste as $slug) {
            $rendus[] = ["item" => $slug, "rendered" => $renderer->toHTML($slug->slug)];
//            dd($renderer->toHTML($slug->slug));
        }


        return view('customfieldsindex', ["data" => $rendus]);

    }

}
