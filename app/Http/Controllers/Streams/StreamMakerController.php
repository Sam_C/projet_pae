<?php

namespace App\Http\Controllers\Streams;

use App\StreamSkeleton;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Stream;
use Illuminate\Support\Facades\Blade;

class StreamMakerController extends Controller
{
    /**
     * @return view, the form to create a customField
     */
    public function createCustomFieldForm()
    {
        $test = new StreamSkeleton;
        $test = $test->getStreamSkeletonCollection();

        dd($test);

        return view('customfields.customFieldCreate');
    }


    /**
     * @param $slug -> url parameter pointing to the customfield
     * @return view -> update form...
     */
    public function updateCustomFieldForm($slug)
    {
        $stream = Stream::all()->where('slug', '=', $slug)->first();
//        dd($stream);

        if ($stream !== null) {

            $rendered = $this->render($slug);
//            dd($rendered);
            return view('customfields.customFieldUpdate', ['data' => $stream, 'preview' => $rendered]);
        }


    }

    /**
     * @param string (used as slug) or array $params:
     *          slug => a string to find in the database, if no record or not a string, f° returns null
     *          value => if null empty string, else value passed as parameter
     *          checked, selected => initial value, if any, and if no conditionalState
     * @return null on error, else HTML String
     */
    public function render($params = [
        'slug' => null,
        'value' => null,
        'checked' => false,
        'label' => false,
        'selected' => false,
        'has-error' => false,
        'conditionState' => null,
        'forceRenderData' => null,
    ])
    {
        //checks if the $params contains a single string or an array
        //if it's a string, we define it as slug
        (is_string($params) ? $params = ['slug' => $params] : null);
        //then we define the defaults values if not filled
        $params['value'] = (isset($params['value'])) ? $params['value'] : null;
        $params['label'] = (isset($params['label'])) ? $params['label'] : null;
        $params['checked'] = (isset($params['checked'])) ? $params['checked'] : false;
        $params['selected'] = (isset($params['selected'])) ? $params['selected'] : false;
        $params['has-error'] = (isset($params['has-error'])) ? $params['has-error'] : false;
        $params['forceRenderData'] = (isset($params['forceRenderData'])) ? $params['forceRenderData'] : false;
        //let's search the stream by it's slug :
        if (is_string($params['slug'])) {
            $stream = Stream::all()->where('slug', '=', $params['slug']);
            //but we can also force rendering with raw code, so we try to define what will be used as HTML
            if ($stream->count() > 0 || $params['forceRenderData'] !== false) {
                //using DB Record
                if ($params['forceRenderData'] == false) {
                    $stream = $stream->first();

                    $html = $stream->render;
                } //or using function's parameter
                else {
                    $html = $params['forceRenderData'];
                }

                //from now on, we only work on HTML,
                //we turn custom tags to values
                $html = str_replace('@slug@', isset($stream->slug) ? $stream->slug : $params['slug'], $html);

                //same with label :
                $html = str_replace('@label@', isset($stream->label) ? $stream->label : $params['label'], $html);

                //inject the "value" tag
                $myreplaceObj = 'value';
                $myreplaceValue = ($params[$myreplaceObj] == null) ? '' : $params[$myreplaceObj];
                $html = str_replace('@' . $myreplaceObj . '@', $myreplaceValue, $html);
                //do we have a boolean value coming from parameters ?
                $params['conditionState'] = (isset($params['conditionState'])) ? $params['conditionState'] : null;
                //if condition state is false, we replace checked / selected by empty string
                if ($params['conditionState'] == null or !is_bool($params['conditionState'])) {

                    $myreplaceObj = 'checked';
                    $myreplaceValue = ($params[$myreplaceObj] == false) ? '' : $params[$myreplaceObj];
                    $html = str_replace('@' . $myreplaceObj . '@', $myreplaceValue, $html);

                    $myreplaceObj = 'selected';
                    $myreplaceValue = ($params[$myreplaceObj] == false) ? '' : $params[$myreplaceObj];
                    $html = str_replace('@' . $myreplaceObj . '@', $myreplaceValue, $html);
                } //if not, we replace them with their values
                else {

                    $myreplaceObj = 'checked';
                    $myreplaceValue = ($params['conditionState'] == false) ? '' : $params[$myreplaceObj];
                    $html = str_replace('@' . $myreplaceObj . '@', $myreplaceValue, $html);

                    $myreplaceObj = 'selected';
                    $myreplaceValue = ($params['conditionState'] == false) ? '' : $params[$myreplaceObj];
                    $html = str_replace('@' . $myreplaceObj . '@', $myreplaceValue, $html);
                }
                //does the field contain an error ? empty string if false, "has-error" bootstrap class if true
                $myreplaceObj = 'has-error';
                $myreplaceValue = ($params[$myreplaceObj] == false) ? '' : $params[$myreplaceObj];
                $html = str_replace('@' . $myreplaceObj . '@', $myreplaceValue, $html);

                //output of the function if we successfully parsed
                $html = Blade::compileString($html);
                //dump($html);

                $obLevel = ob_get_level();

                ob_start();

                // On capture les erreurs de l'eval()
                $error = false;
                set_error_handler(function ($e) use (& $error) {

                    $error = true;
                });

                // On remplace le code php pouvant être interprété
                $response = eval('?' . '>' . $html);


                // Rétablissement de la capture
                restore_error_handler();

                // On vérifie si il y a eu des erreurs
                if ($error) {

                    // On vide le tampon et on affiche l'erreur
                    ob_clean();
                    echo "Fatal error, unable to perform blade render";
                }


                // On retourne la chaine de caractère résultante

                return ob_get_clean();


                //return $html;
                //dd($html);
            }
            //else, we return null, in case there was nothing to render (from DB or from params
            return null;
        } else return null;


    }

    public function updateCustomFieldStore($slug, Request $request)
    {
        //TODO : beware of slug rename !!
        $stream = Stream::all()->where('slug', '=', $slug)->first();

        if ($stream !== null) {
//            dump($stream);

            $this->validate($request, [
                'streamName' => 'required|string|min:3',
                'streamSlug' => 'string|min:3',
                'streamlabel' => 'string|nullable',
            ],
                [
                    'streamName.required' => 'Le nom du champs personnalisé doit être renseigné',
                    'streamSlug.unique' => 'Le slug du champs personnalisé doit être unique',
                ]
            );

            $stream->name = $request->request->filter('streamName');
//            $stream->slug = $request->request->filter('streamSlug');
            $stream->label = $request->request->filter('streamLabel');
            $stream->validator = $request->request->filter('streamValidationRule');
            $stream->render = $request->request->filter('formHTML');
//            dd($stream);
            $stream->save();

            return redirect(route('customfield.update.store', $stream->slug));
        }


    }

    public function store(Request $request)
    {
//        dump($request);

        $this->validate($request, [
            'streamName' => 'required|string|min:3',
            'streamSlug' => 'string|min:3|unique:streams,slug',
            'streamLabel' => 'required|string|min:1',
//            '' => '',
        ],
            [
                'streamName.required' => 'Le nom du champs personnalisé doit être renseigné',
                'streamSlug.unique' => 'Le slug du champs personnalisé doit être unique',
            ]
        );

        $str = new Stream;
        $str->name = $request->request->filter('streamName');
        $str->slug = $request->request->filter('streamSlug');
        $str->label = $request->request->filter('streamLabel');
        $str->validator = $request->request->filter('streamValidationRule');
        $str->render = $request->request->filter('formHTML');

        $str->save();
        return $this->index();

    }

    public function index()
    {
        $streams = Stream::all();



        $data = array();
        $item = array();
        foreach ($streams as $stream) {
            $item['item'] = $stream;
            $item['rendered'] = $this->render($stream->slug);
            $data[] = $item;
        }

        return view('customfields.customfieldsindex', ['data' => $data]);
    }

    public function ajaxCheckSlugIsUnique($slug)
    {
        if ($slug == null || $slug == "") {
            return json_encode(1);
        } else {
            $res = Stream::all()->where('slug', '=', $slug)->count();
            return (json_encode($res));
        }

    }

    public function delete(Request $request)
    {
//        dd($request);
        $stream = Stream::all()->where('slug', '=', $request->request->filter('slug'))->first();
        if ($stream !== null) {
            $stream->delete();
            return redirect()->back()->with('success', 'Champ perso. effacé');
        } else
            return redirect()->back()->with('errors', 'erreur lors de la suppression');
    }

    public function postRender(Request $request)
    {
//        dump($request);
        //first, we escape the text tags from ACE editor
//        dump($this->render(['slug' => $request->input('slug'),'forceRenderData'=>$request->input('forceRenderData')]));
        $str = str_replace(array("\t", "\n", "\r"), '', $this->render([
            'slug' => $request->input('slug'),
            'forceRenderData' => $request->input('forceRenderData'),
            'label' => $request->input('label'),
        ]));
        //dump($str);

        //we escape the escaping chars left behind quotes (I.E)

        //then we return a JSON encoded string.

        return ($str);
    }


}
