<?php

namespace App\Http\Controllers\CampaignMgt;

use App\Campaign;
use App\Http\Controllers\Streams\StreamController;
use App\Stream;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CampaignController extends Controller
{
    //
    
    public function index(){
        
    }

    public function creationForm(){
        return view('campaigns.campaigncreate');
    }
    
    public function store(Request $request){
        dump($request);

        $this->validate($request, [
//            'name' => 'nullable',
//            'my_name' => 'nullable',
//            'firstname' => 'nullable',
//            'lastname' => 'nullable',
//            'what_is_the_airspeed_velocity_of_an_unladen_swallow' => 'string',
        ]);

        $timestamp = decrypt($request->request->filter('what_is_the_airspeed_velocity_of_an_unladen_swallow'));
        if (is_integer($timestamp)) {
            if (time() - $timestamp < 5)
                //TODO (or not...) : form was submitted too fast
                dump('You are really fast !');
        }
        if ($request->request->filter('name') != null
            || $request->request->filter('my_name') != null
            || $request->request->filter('firstname') != null
            || $request->request->filter('lastname') != null
        ) {
            //todo (or not...) : some honeypot fields have been modified
            dump('Invisible Pink Unicorn ?');
        }
    }
    
    public function update(Request $request){
        
    }
    
    public function delete(Request $request){
        
    }

    public function render()
    {
        $campaign = Campaign::all()->first();
        $streamRender = new StreamController;

        $html = $campaign->html_form;

        $elements = explode('$', $html);
        $items = [];
        foreach ($elements as $index => $element) {
            if ($element == "honeypot") {
                $timestamp = encrypt(time());
                $items[] = '<div style="display:none;">'
                    . '  <input type="text" name="name" id="name">'
                    . '  <input type="text" name="my_name" id="my_name">'
                    . '  <input type="text" name="firstname" id="firstname">'
                    . '  <input type="text" name="lastname" id="my_name">'
                    . '  <input type="hidden" name="what_is_the_airspeed_velocity_of_an_unladen_swallow" id="what_is_the_airspeed_velocity_of_an_unladen_swallow" value="' . $timestamp . '">'
                    . '</div>';

            } else {
                $customField = Stream::all()->where('slug', '=', $element);
                if ($customField->isNotEmpty()) {
                    if ($customField->first()->slug == $element) $items[] = $streamRender->render($customField->first()->slug);
                } else $items[] = $element;
            }
        }
        return view('campaigns.formrender', ["elements" => $items]);
    }


}
