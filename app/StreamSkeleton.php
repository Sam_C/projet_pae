<?php

namespace App;


    /*
     * derive any Skeleton class from StreamSkeleton
     * modify it's constructor so it reflects what you want
     *
     *
     */


/**
 * Class StreamSkeleton
 * @package App
 *
 * Enumerates children of StreamSkeleton_mother class as a readonly array
 */
class StreamSkeleton
{
    private $streamSkeletonCollection = array();

    public function __construct()
    {
        //go through all the known classes
        foreach (get_declared_classes() as $class) {
            if (is_subclass_of($class, 'App\\StreamSkeleton_mother')) //get only children
            {
                $this->streamSkeletonCollection[] = new $class; //append it to the array
            }
        };
        //dd($this->streamSkeletonCollection);

    }

    public function getStreamSkeletonCollection()
    {
        return $this->streamSkeletonCollection;
    }

}

/**
 * Class StreamSkeleton
 * @package App
 *
 * is the mother of any of the StreamSkeleton_ classes, in any child classe, you just have to mod the constructor
 */
class StreamSkeleton_mother
{
    protected $html = ""; //will store the HTML code
    protected $trivial_name = ""; //will store a trivial name to be displayed in IHM


    public function __construct()
    {
        //input your HTML code in $tmp
        $tmp = null;
        $this->setHtml($tmp);

    }


    //accessors, these wont be in children classes, thus they are declared as final.
    final public function getHtml()
    {
        return $this->html;
    }

    final protected function setHtml($html)
    {
        $this->html = $html;
    }

    final public function getTrivialName()
    {
        return $this->trivial_name;
    }

    final public function setTrivialName($trivial_name)
    {
        $this->trivial_name = $trivial_name;
    }

}

class StreamSkeleton_text extends StreamSkeleton_mother
{
    protected $html;
    protected $trivial_name = "Text Field";

    public function __construct()
    {

        $tmp = '<div id="" class="control-group {{$errors->has("@slug@" ? "has-error" : ""}}>\n'
            . '\t<label for="@slug@">@label@</label>\n'
            . '\t<input id="@slug@" name="@slug@" type="text" value="{{old("value") or isset($value) ? $value : ""}}">\n'
            . '</div>';

        $this->setHtml($tmp);

    }

}

class StreamSkeleton_date extends StreamSkeleton_mother
{
    protected $html;
    protected $trivial_name = "Date Field";

    public function __construct()
    {

        $tmp = '<div id="" class="control-group {{$errors->has("@slug@" ? "has-error" : ""}}>\n'
            . '\t<label for="@slug@">@label@</label>\n'
            . '\t<input id="@slug@" name="@slug@" type="date" value="{{old("value") or isset($value) ? $value : ""}}">\n'
            . '</div>';

        $this->setHtml($tmp);

    }

}

class StreamSkeleton_datetime extends StreamSkeleton_mother
{
    protected $html;
    protected $trivial_name = "Date/Time Field";

    public function __construct()
    {

        $tmp = '<div id="" class="control-group {{$errors->has("@slug@" ? "has-error" : ""}}>\n'
            . '\t<label for="@slug@">@label@</label>\n'
            . '\t<input id="@slug@" name="@slug@" type="datetime-local" value="{{old("value") or isset($value) ? $value : ""}}">\n'
            . '</div>';

        $this->setHtml($tmp);

    }

}

class StreamSkeleton_email extends StreamSkeleton_mother
{
    protected $html;
    protected $trivial_name = "Date Field";

    public function __construct()
    {

        $tmp = '<div id="" class="control-group {{$errors->has("@slug@" ? "has-error" : ""}}>\n'
            . '\t<label for="@slug@">@label@</label>\n'
            . '\t<input id="@slug@" name="@slug@" type="email" value="{{old("value") or isset($value) ? $value : ""}}">\n'
            . '</div>';

        $this->setHtml($tmp);

    }

}

